/*
Node is defined as 
  class Node {
     int data;
     Node next;
  }
*/

Node RemoveDuplicates(Node head) {
  // This is a "method-only" submission. 
  // You only need to complete this method. 
  Node node = new Node();
  node = head;
  while(node.next != null){
      if(node.data != node.next.data){
          node = node.next;
      }
      else{
          node.next = node.next.next;
      }
  }
    
    return head;
}
