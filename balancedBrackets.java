import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        //System.out.println("N is "+n);
        for(int i = 0; i <= n; i++){
            String s = sc.next();
            //System.out.println("String is "+s);
            Stack<Character> st = new Stack();
            for(int j = 0; j < s.length(); j++){
                //System.out.println(s.length());
                if(s.length() % 2 != 0){
                    System.out.println("NO");
                }
                
                else{
                    if(st.isEmpty()){
                        //System.out.println("Character added");
                        st.push(s.charAt(j));
                    }
                    else if(!st.isEmpty()){
                        if(s.charAt(j) == ']'){
                            if(st.peek() == '['){
                                st.pop();
                                //System.out.println("Stack is "+st.size());
                            }
                        }
                        else if(s.charAt(j) == '}'){
                            if(st.peek() == '{'){
                                st.pop();
                                //System.out.println("Stack is "+st.size());
                            }
                        }
                        else if(s.charAt(j) == ')'){
                            if(st.peek() == '('){
                                st.pop();
                                //System.out.println("Stack is "+st.size());
                            }
                        }
                        else{
                            //System.out.println("Character added to stack");
                            st.push(s.charAt(j));
                        }
                    }
                    
                }
                //System.out.println(st);
                if(st.isEmpty() && j == s.length()-1){
                        System.out.println("YES");
                    }
                else if(j == s.length()-1 && !(st.isEmpty())){
                        System.out.println("NO");
                }
            }
        }
    }
}
