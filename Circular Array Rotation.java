import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int q = in.nextInt();
        int[] a = new int[n];
        int[]rotated = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
            //System.out.println(a[a_i]);
        }
        
        for(int i = 0; i < n;i++){
            int value = a[i];
            rotated[(i+k)%n] = value;
        }
        
        
        for(int a0 = 0; a0 < q; a0++){
            int m = in.nextInt();
            System.out.println(rotated[m]);
        }
    }
}
