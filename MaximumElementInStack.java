import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        Stack<Integer> st = new Stack();
        Stack<Integer> track = new Stack();
        int n = sc.nextInt();
        for(int i = 0; i < n; i++){
            int v1 = sc.nextInt();
            if(v1 == 1){
                int v2 = sc.nextInt();
                //System.out.println(v2);
                if(st.isEmpty()){
                st.add(v2);
                track.add(v2);
                }
                else{
                    st.add(v2);
                    int v = track.peek();
                    int value = Math.max(v2,v);
                    track.add(value);
                }
                   
                
                
            }
            if(v1 == 2){
                st.pop();
                track.pop();
            }
            else if(v1 == 3){
                System.out.println(track.peek());
            }
                
        }
    }
}