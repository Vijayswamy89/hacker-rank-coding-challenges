import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        //System.out.println("The number of cases is " +n);
        while(sc.hasNextLine()){
            int count = 0;
            String s = sc.nextLine();
            //System.out.println("Each line of the document is " + "\n" +s);
            //System.out.println("The length of string is "+s.length());
            if(s.length()%2!=0){
                System.out.println(-1);
            }
            else{
                String s1 = s.substring(0,s.length()/2);
                //System.out.println("The first half of the substring is "+s1);
                String s2 = s.substring(s.length()/2,s.length());
                //System.out.println("The second half of the substring is " +s2);
                ArrayList<Character> c1 = new ArrayList<Character>();
                ArrayList<Character> c2 = new ArrayList<Character>();
                
                for(int i = 0; i<s1.length();i++){
                    c1.add(s1.charAt(i));
                }
                
                for(int i = 0; i<s2.length();i++){
                    c2.add(s2.charAt(i));
                }
                
                Collections.sort(c1);
                Collections.sort(c2);
                //System.out.println("c1 arraylist length: "+ c1.size());
                //System.out.println("c2 arraylist length: "+ c2.size());                
                for(int j = 0; j < c1.size();j++){
                    if(!(c2.contains(c1.get(j)))){
                        count = count + 1;
                    }
                    else{
                        c2.remove(c1.get(j));
                    }
                }
                }
                if(s.length()%2==0){
                 System.out.println(count);   
                }
            }
        }
    }