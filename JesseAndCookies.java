import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        boolean valid = true;
        int count = 0;
        int n = sc.nextInt();
        int k = sc.nextInt();
        PriorityQueue <Integer>  p = new PriorityQueue <Integer> ();
        while(sc.hasNextInt()){
            int v = sc.nextInt();
            //System.out.println(i);
            p.add(v);
        }
        while(p.peek() < k && p.size()>=2){
            int s1 = p.poll();
            int s2 = p.poll();
            //System.out.println("First integer removed is "+s1);
            //System.out.println("Second integer removed is "+s2);
            int value = s1 + 2*(s2);
            //System.out.println("Computed value is "+value);
            p.add(value);
            count = count + 1;
        }
        if(p.peek() < k){
            count = -1;
        }
        System.out.println(count);
        
    }
}