import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        StringBuffer sb = new StringBuffer();
        while(sc.hasNext()){
            sb.append(sc.next());
        }
        char[] c = sb.toString().toLowerCase().toCharArray();
        Set resultSet = new HashSet();
        for(char c1: c){
            resultSet.add(c1);
        }
        String output = (resultSet.size() == 26)?"pangram":"not pangram";
        System.out.println(output);
    }
}